# Description
It is a python 🐍 kata 🥋 to practice TDD 🧪 (test driven development)

![Github](https://github.com/zearkiatos/python-kata/actions/workflows/action.yml/badge.svg)

# Make with
[![Python](https://img.shields.io/badge/python-2b5b84?style=for-the-badge&logo=python&logoColor=white&labelColor=000000)]()